#!/usr/bin/env python3

"""
Create Epub files.

This code was designed to provide a very simple and straight-forward API for
creating epub files, by sacrificing most of the versatility of the format.
"""

###############################################################################
# Module Imports
###############################################################################

import collections
import datetime
import imghdr
import itertools
import jinja2
import pathlib
import tempfile
import uuid
import zipfile


###############################################################################

def mediatype(name):
    ext = name.split('.')[-1].lower()
    if ext not in ('png', 'jpg', 'jpeg', 'gif', 'svg'):
        raise ValueError('Image format "{}" is not supported.'.format(ext))
    if ext == 'jpg':
        ext = 'jpeg'
    return 'image/' + ext


def fonttype(name):
    ext = name.split('.')[-1].lower()
    mimetypes = {
        'otf': 'application/font-sfnt',
        'ttf': 'application/font-sfnt',
        'woff': 'font/woff',
        'woff2': 'font/woff2',
    }
    if ext not in mimetypes.keys():
        raise ValueError('Font format "{}" is not supported.'.format(ext))
    return mimetypes[ext]


###############################################################################

templates = {

    # container.xml
    'container.xml': '''\
<?xml version="1.0" encoding="utf-8" standalone="no"?>
<container xmlns="urn:oasis:names:tc:opendocument:xmlns:container" version="1.0">
    <rootfiles>
        <rootfile full-path="EPUB/package.opf" media-type="application/oebps-package+xml"/>
    </rootfiles>
</container>
''',

    # cover.xhtml
    'cover.xhtml': '''\
<?xml version="1.0" encoding="utf-8" standalone="no"?>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" xml:lang="en" lang="en">
    <head>
        <title>Cover</title>
        <style type="text/css">
            img{
                max-width:100%;
            }
        </style>
    </head>
    <body>
        <figure id="cover-image">
            <img src="covers/{{ cover }}"/>
        </figure>
    </body>
</html>
''',

    # package.opf
    'package.opf': '''\
<?xml version="1.0" encoding="utf-8" standalone="no"?>
<package xmlns="http://www.idpf.org/2007/opf" xmlns:dc="http://purl.org/dc/elements/1.1/"
  xmlns:dcterms="http://purl.org/dc/terms/" version="3.0" xml:lang="en"
  unique-identifier="uuid">
  <metadata>
    <dc:identifier id="uuid">{{ uuid }}</dc:identifier>
    <dc:title>{{ title|escape }}</dc:title>
    <dc:language>{{ lang if lang else 'en' }}</dc:language>
    <dc:date>{{ published if published else date[:10] }}</dc:date>
    <meta property="dcterms:modified">{{ date }}</meta>
    {%- if author %}
    <dc:creator>{{ author|escape }}</dc:creator>
    {%- endif %}
    {%- for author in authors %}
    <dc:creator>{{ author|escape }}</dc:creator>
    {%- endfor %}
    {%- for subject in subjects %}
    <dc:subject>{{ subject|escape }}</dc:subject>
    {%- endfor %}
    {%- if description %}
    <dc:description>{{ description|escape }}</dc:description>
    {%- endif %}
    {%- if rights %}
    <dc:rights>{{ rights|escape }}</dc:rights>
    <meta name="cover" content="cover"/>
    {%- endif %}
  </metadata>
  <manifest>
    <item id="htmltoc" properties="nav" media-type="application/xhtml+xml" href="toc.xhtml"/>
    <item href="toc.ncx" id="toc" media-type="application/x-dtbncx+xml"/>
    <item media-type="text/css" id="css" href="css/stylesheet.css"/>
    {%- if cover %}
    <item id="cover-xhtml" href="cover.xhtml" media-type="application/xhtml+xml"/>
    <item id="cover" properties="cover-image" href="covers/{{ cover }}" media-type="{{ cover|mediatype }}"/>
    {%- endif %}
    {%- for page in pages %}
    <item id="id-{{ page.page_id }}" href="page{{ page.page_id }}.xhtml" media-type="application/xhtml+xml"/>
    {%- endfor %}
    {% for image in images %}
    <item id="id-im{{ image.image_id }}" href="images/{{ image.name }}" media-type="{{ image.name|mediatype }}"/>
    {% endfor %}
    {%- for font in fonts %}
    <item id="id-{{ font }}" href="fonts/{{ font }}" media-type="{{ font|fonttype }}"/>
    {%- endfor %}
  </manifest>
  <spine toc="toc">
    {%- if cover %}
    <itemref idref="cover-xhtml" linear="yes"/>
    {%- endif %}
    {%- for page in pages %}
    <itemref idref="id-{{ page.page_id }}"/>
    {%- endfor %}
  </spine>
</package>
''',

    # page.xhtml
    'page.xhtml': '''\
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" xml:lang="en" lang="en">
  <head>
    <title>{{ title }}</title>
    <link href="css/stylesheet.css" rel="stylesheet" type="text/css"/>
  </head>
  <body>
    {{ body }}
  </body>
</html>
''',

    # toc.ncx
    'toc.ncx': '''\
<?xml version="1.0" encoding="UTF-8"?>
<ncx xmlns:ncx="http://www.daisy.org/z3986/2005/ncx/" xmlns="http://www.daisy.org/z3986/2005/ncx/"
    version="2005-1" xml:lang="en">
    <head>
        <meta name="dtb:uid" content="{{ uuid }}"/>
        <ncx:meta name="dtb:totalPageCount" content="{{ pages|length }}"/>
    </head>
    <docTitle>
        <text>{{ title }}</text>
    </docTitle>
    <navMap>
        {%- for page in pages recursive %}
        <navPoint id="id-{{ page.page_id }}" playOrder="{{ page.page_id.lstrip('0') }}">
            <navLabel>
                <text>{{ page.title }}</text>
            </navLabel>
            <content src="page{{ page.page_id }}.xhtml"/>
            {%- if page.children %}
            {{ loop(page.children) }}
            {%- endif %}
        </navPoint>
        {%- endfor %}
    </navMap>
</ncx>
''',

    # toc.xhtml
    'toc.xhtml': '''\
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" xml:lang="en"
  lang="en">
  <head>
    <title>{{ title }}</title>
    <link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
  </head>
  <body>
    <h1>{{ title }}</h1>
    <nav epub:type="toc" id="toc">
      <h2>Table of Contents</h2>
      <ol>
      {%- for page in pages recursive %}
        <li>
          <a href="page{{ page.page_id }}.xhtml">{{ page.title }}</a>
          {%- if page.children %}
          <ol>{{ loop(page.children) }}</ol>
          {%- endif %}
        </li>
      {%- endfor %}
      </ol>
    </nav>
  </body>
</html>
'''
}

env = jinja2.Environment(loader=jinja2.DictLoader(templates))
env.filters['mediatype'] = mediatype
env.filters['fonttype'] = fonttype

###############################################################################


class Page(object):
    def __init__(self, parent, page_id, title, content=''):
        self.parent = parent
        self.page_id = page_id
        self.title = title
        self.children = []
        self.content = content

    def write(self, content):
        self.content += content

    def close(self):
        if self.content is not None:
            self.parent.close_page(self)
            self.content = None


Image = collections.namedtuple('Image', 'image_id name')


class Book:
    """EPUB book."""

    def __init__(self, title, **metadata):
        """"Create new book."""
        self.title = title
        self.metadata = metadata

        self.tempdir = tempfile.TemporaryDirectory()
        self.root = []
        self.fonts = []
        self.images = []
        self.uuid = uuid.uuid4()
        self._page_id = map('{:04}'.format, itertools.count(1))
        self._image_id = map('{:03}'.format, itertools.count(1))
        self._images = {}

        self.path = pathlib.Path(self.tempdir.name).resolve()
        for dirname in [
                'EPUB', 'META-INF', 'EPUB/images', 'EPUB/css', 'EPUB/covers']:
            (self.path / dirname).mkdir()

        self.set_stylesheet('')
        self._cover = None

    ###########################################################################
    # Public Methods
    ###########################################################################

    def add_page(self, title, content='', parent=None):
        """
        Add a new page.

        The page will be added as a subpage of the parent. If no parent is
        provided, the page will be added to the root of the book.
        """
        page = Page(self, next(self._page_id), title, content=content)
        self.root.append(page) if not parent else parent.children.append(page)
        return page

    def close_page(self, page):
        if page.content is not None:
            self._write_page(page)
            page.content = None

    def add_image(self, name, data):
        """Add image file."""
        try:
            return self._images[name]
        except KeyError:
            self.images.append(Image(next(self._image_id), name))
            dest = pathlib.Path('images') / name
            self._add_file(dest, data)
            dest = str(dest)
            self._images[name] = dest
            return dest

    def add_font(self, name, data):
        """Add font file."""
        self.fonts.append(name)
        self._add_file(pathlib.Path('fonts') / name, data)

    def set_cover(self, data):
        """Set the cover image to the given data."""
        self._cover = 'cover.' + imghdr.what(None, h=data)
        self._add_file(pathlib.Path('covers') / self._cover, data)
        self._write('cover.xhtml', 'EPUB/cover.xhtml', cover=self._cover)

    def set_stylesheet(self, data):
        """Set the stylesheet to the given css data."""
        self._add_file(
            pathlib.Path('css') / 'stylesheet.css', data.encode('utf-8'))

    def save(self, filename):
        """Save book to a file."""
        if pathlib.Path(filename).exists():
            raise FileExistsError
        self._write_spine()
        self._write('container.xml', 'META-INF/container.xml')
        self._write_toc()
        with open(str(self.path / 'mimetype'), 'w') as file:
            file.write('application/epub+zip')
        with zipfile.ZipFile(filename, 'w') as archive:
            archive.write(
                str(self.path / 'mimetype'), 'mimetype',
                compress_type=zipfile.ZIP_STORED)
            for file in self.path.rglob('*.*'):
                archive.write(
                    str(file), str(file.relative_to(self.path)),
                    compress_type=zipfile.ZIP_DEFLATED)

    ###########################################################################
    # Private Methods
    ###########################################################################

    def _add_file(self, name, data):
        """Add a file."""
        filepath = self.path / 'EPUB' / name
        if not filepath.parent.exists():
            filepath.parent.mkdir()

        with open(str(filepath), 'wb') as file:
            file.write(data)

    def _write(self, template, path, **data):
        with open(str(self.path / path), 'w') as file:
            file.write(env.get_template(template).render(**data))

    def _write_page(self, page):
        """Write the contents of the page into an html file."""
        self._write(
            'page.xhtml', 'EPUB/page{}.xhtml'.format(page.page_id),
            title=page.title, body=page.content)

    def _write_spine(self):
        self._write(
            'package.opf', 'EPUB/package.opf',
            title=self.title,
            date=datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ'),
            pages=list(self._flatten(self.root)), images=self.images,
            fonts=self.fonts, uuid=self.uuid, cover=self._cover,
            **self.metadata)

    def _write_toc(self):
        self._write(
            'toc.xhtml', 'EPUB/toc.xhtml', pages=self.root, title=self.title)
        self._write(
            'toc.ncx', 'EPUB/toc.ncx',
            pages=self.root, title=self.title, uuid=self.uuid)

    def _close_all(self):
        for page in self._flatten(self.root):
            self.close_page(page)

    def _flatten(self, tree):
        for item in tree:
            yield item
            yield from self._flatten(item.children)
